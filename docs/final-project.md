# **Final Project**


On this page of my site, i will develop the final project i am working on with my group, in collaboration with the Bruxelles's Design Museum.

## **Kids@BDM Carte à jouer**.

Base on our visit at the Bruxelles's Design Museum and the meetings we had with the museum's staff, me and my group composed with Tejhay Albia and Farah Mesbahi had to think, design and produced workshop aim to entertain differents group of childrens aged 6 years old to 12 years old.

Tejhay was the first to work on this project will we had to follow the lessons about the different machine we are using to the FabLab. 
As first he remembered his childhood where he was playing different games. He thought it was better to design a game for children while thinking like a child. 
He remembered when he cas playing with cards games in his school's court with his friends. He decided to work on a game card as a playful educational project for the childrens. 

When we joined him to help and work with him, bringing with us a new point of view and new ideas. At this time, he already had designed some cards and card's holder. 

The main goal of this game is to walk across the museum with our card's holder around the museum. Thoses cards have hole with the form of the object we are lookig for. 

**Images taken in the museum with our first prototype of card**
![](docs/images/carte.jpg)

At the bottom of the object you just find, a badges's holder is placed. You have to take one of the badges and then plug it on you're cards's holder. We did nine type of different badges, one for each type of plastic present in the museum. 

**One of the cards's holder we designed**
![](docs/images/testboite4.jpg)

At the end of the badges research and you're cards's holder totally completed with all of the nine badges, you have to give it back to the person in charge of this workshop. At first, no reward where given to childrens. I thought it would be enjoyable for the kids to have a sort of reward. 
I first think that a coin could be great, like the memorial coins we get to a monument or things like that. I designed a coin that could be printed in 3D but also which could be cut. 

Farah writed a story around the museum and our game, attracting the kid by giving them a role. Here is farah counting the story and the rules to differents child.

![](docs/images/enfant2.jpg)


## **Cards** 

As first, Tejhay tried differents formats and dimensions for the cards. 

![](docs/images/carte.jpg)

Then we had to choose a specific format for our cards. As first we choosed a **92mm heigh by a 67mm width** but it was to close of the format that used a famous iconic game card. So we decided to withdraw **2mm** of heigh and width to our card, the final format will be **90mm heigh and 65mm width**.

We wanted to had a context to our card, the context needed to be the same as the year the furniture was created. We start by doing our card's template, then we could cut out the shape of each objects and after that to draw the context of each cards. 

![](docs/images/template.jpg)

On every card we added the name of the object and his creator, the badges of the type's platic, the year of creation of the object and some particularity and weakness of every object and type of plastics. 

For the context as i said we used pictures that fit with the year of creation of the object.



**Blue endless chair's card drawing**
![](docs/images/template6.jpg)





**Airchair's card drawing**
![](docs/images/template2.jpg)





**Aero space's card drawing**
![](docs/images/template4.jpg)





**master chair's card drawing**
![](docs/images/template5.jpg)





**Torch Bunch's card drawing**
![](docs/images/template1.jpg)





**Universale's card drawing**
![](docs/images/template7.jpg)




**Elephant Chair's card drawing**
![](docs/images/template3.jpg)

Here are the result of what cards are looking when it's finally printed on a cardboard paper. I will also write the settings i've been using and what type of carboard i used for those cards. 

**Universale chair's prototype card**
![](docs/images/carte2.jpg)

**Master chair's prototype card**
![](docs/images/carte1.jpg)

**Elephant Chair's prototype card**
![](docs/images/carte3.jpg)

**Torch bunch's prototype card**
![](docs/images/carte4.jpg)

**Tejhay's prototype cards**
![](docs/images/carte5.jpg)

We used for those cards a 1 millimeter grey carboard and here are the seetings for those cards.

**Settings for prototype card**
![](docs/images/carteset2.jpg)

At the end of the third week of our project we had a meeting with our teacher. The result of that meeting was that our card's context were to much charged, we had to simplify it, especially for our main target for this game. Childrens could had trouble to understand the complexity of the context. It needed to simplify to have a clearer and understandable point of view. 

We also decided to simplify the informations available on the cards. On our first prototype there were differents type of informations marked so as :

- The name of the object
- Designer's name of the object
- The plastic type
- The year of the object creation
- Some weakness and particularity of the object and/or the type of plastic.

To simplify the card and it's readabilty we decided to delete the weakness and the particularitys and to write it on our badges's holders. 


**Master chair's second prototype drawing**
![](docs/images/c1.jpg)

**Blue endless chair's second prototype drawing**
![](docs/images/c2.jpg)

**Universale chair's second prototype drawing**
![](docs/images/c3.jpg)

**Aerospace's second prototype drawing**
![](docs/images/c4.jpg)

**Air chair's second prototype drawing**
![](docs/images/c5.jpg)

**Torch bunch's second prototype drawing**
![](docs/images/c6.jpg)

**Universale chair's second prototype drawing**
![](docs/images/c7.jpg)

For our meeting with the kids and they're first encounter with our game we printed some of our card but not totally, picture underneath is the result of the first try. We used a different 

**Encounter with the kids card**
![](docs/images/c8.jpg)

We gonna have to reprinted all the card with a new cardboard woth a double face on the 12 wenesday of December 2021. 

Here is all of our work on the cards and all the prototype we made at this time. 

![](docs/images/meet1.jpg)

_Ps: they're not all on the table but we did way to much cards, i would probably never play with cards in my entire life._

After our jury in December we decided to not rework our car's design, just to adjust some little details and to add the museum's logo on the back of the cards. 

**Final cutting of the cards**
![](docs/images/dos4.jpg)

**engraving the museum logo**
![](docs/images/dos1.jpg)

**engraved museum's logo**
![](docs/images/dos2.jpg)

We had some fails while engraving the logo on the back of the cards because the machine is not precise enough to not have to make some try and adjustments. 

**Failed card**
![](docs/images/dos3.jpg)

**we had to moove a  little bit the logo out of the card on the program to be able to print it correctly**
![](docs/images/dos5.jpg)

## **Cards's Holder**

Our game includes a cards holder in which card are gonna get into and badges are gonna plug into. Once the child find the object he can open the badges chest and take one of the badges which represent the type of the object's plastic. Once he get those badges he can plug it on the cards holder and keep on his design trip. When kids plug all of the 10 badges they have to find, the game is over and they can get they're rewind that we talk about above. 

As first, the cards holders were just box where the cards where tidy and the badges plugged but we had to hold it with our hand. We needed to find a way to hand it on players. We thought of a hook system that can hand on the pants or the pocket.

We also needed to assembler the cards holder without any glue or anything. We thought about a tooth system, like that we could plug every face of the holder together. We had to well think this system and the tooth's size. The female tooth are 0.15mm less tall than the male tooth. With this width the tooths are fitting together creating a solid box. 

We tryed different width for the tooth but didn't work as well as 0.15 mm difference 

**Pieces fitted but the space was to tall and they didn't stayed together**
![](docs/images/boitefail.jpg)


**Some layers of the svg wasn't good so the epilog didn't cut all of the pieces.**
![](docs/images/boitefail3.jpg)


We used as first 3 mm plexiglass waste but we will soon miss of plexiglass so we get new waste in a Plexiglass company based in Charleroi call **Creaplex**, a big thank you to let us take piece of plexi that allows us to keep our work going on. Here is they're google page if you want to contact them.


**Drawing of the holder prorotype**
![](docs/images/boitedessin.jpg)


**Creaplex**
![](docs/images/creaplex.jpg)

As first we tried our system to hang the holder to a pant. 

**Test of the holder on myself**
![](docs/images/testboite1.jpg)

**Test of the holder on myself**
![](docs/images/testboite4.jpg)

**Test of the holder on myself**
![](docs/images/testboite10.jpg)

**Test of the holder**
![](docs/images/testboite9.jpg)

We then add the bottom piece to our holder and plug all of our badges into the holder. we could now have a pre-final vision of what the holder will look completely full at the end of the game.

**Prototype of the holder with badges**
![](docs/images/testbadges.jpg)

**Prototype of the holder with badges**
![](docs/images/testboite2.jpg)

**Prototype of the holder with badges**
![](docs/images/testboite3.jpg)

**Prototype of the holder with badges**
![](docs/images/testboite5.jpg)

**Prototype of the holder with badges**
![](docs/images/testboite6.jpg)

**Prototype of the holder with badges**
![](docs/images/testboite7.jpg)

**Prototype of the holder with badges**
![](docs/images/testboite8.jpg)

After a few tests and a meeting with kids we raised several points that we could chage:

-The edges of the holder were to sharp, they hurt and are annoying.
-Some of the badges are difficult to plug into holders
-The box is to large for kids pants
-Kids keep the box in the hand because it's easier to keep it and plug badges instead of taking it off and putting it back every time they had to plug a new badges.
-The holders need to be separate in two part, one for the used card and one for the lefting cards.

**A kid with the holder cutting in a 3 mm wood plank**
![](docs/images/enfant9.jpg)

**Another kid with a holder printed in 3D who hold the 3 mm plexiglass**
![](docs/images/enfant8.jpg)

After that crash test with the kids we decided to change some of the points of the holders. 

As first we changed the hanging system of the holder: It will be easier and usefull to hang holders to a shoulder strap rather than to hang it to the pant. We will use two carabiner to hang the holder to a recover tarpaulin as a shoulder strap. We can embroider the museum's logo on the tarpaulin to had something recognizable. 

We made some tries to know the spacing between the stitches. Here, the logo was to small and the spacing was to dense.
![](docs/images/broderie4.jpg)

![](docs/images/broderietest1.jpg)

We also separate the holder is two different space as i said above. One of the part will be for the card who are already been found and one for the 
cards who still remain to be found.

There is the last prototype holders we made with the new hanging system.

**Final hanging system for the cards holders**
![](docs/images/carteboite.jpg)

The final cards holder will be printed in 3D with a system to clips the face of plexiglass where will be plugged the badges.

I worked on the new holding system by printing new hook and by embroiding the museum's logo on a strap. 


**Hooks I printed**
![](docs/images/crochet1.jpg)


**Hooks I printed**
![](docs/images/crochet2.jpg)

**Straps that i'd embroiding**
![](docs/images/bande2.jpg)

**Straps that i'd embroiding**
![](docs/images/bande1.jpg)

**Teajhay wearing the cards holder**
![](docs/images/tj1.jpg)

More information of the lattest updates on Farah's git : https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/farah.mesbahi/-/blob/main/docs/final-project.md

## **Badges's chest**

In our game, once the object is find, you will have to find the badges chest, this chest are containing all of the 10 badges types, every type is repesenting every type of plastic that composing every of our 21 object that are separted in two group. Every group is composed by 9 differents object and one who's similar for the two groups, this object is the training one, kids are learning the aim of the game with this object (Joe colombo's Universale Chair).

**Plan of the different objects and location of the badges chests*
![](docs/images/plan.jpg)

We first thought that those chest must be connected to the rest of the game. The concept was to open the chest with the cards. For that we needed that the hole we made in the card can fit with the chest. For that we used the same pieces that we gave in rewards to the kids, creating a hole on the top of our chest to plug those piece into. We used the same fixing system of the tooth to plug every pieces.

**Blue endless chair plug test**
![](docs/images/plug.jpg)

**Blue endless chair plug test**
![](docs/images/plug2.jpg)

**Blue endless chair plug test**
![](docs/images/plug3.jpg)

**Blue endless chair plug test**
![](docs/images/plug4.jpg)

I then draw all of the 5 top of those chest for every object we presented to the kids. We had the informations that we deleted from the cards on ou chest.


**Universale chair's chest's top**
![](docs/images/t1.jpg)

**Blue endless chair's chest's top**
![](docs/images/t2.jpg)

**Aerospace's chest's top**
![](docs/images/t3.jpg)

**Air chair's chest's top**
![](docs/images/t4.jpg)

**Torch bunch's chest's top**
![](docs/images/t5.jpg)

Here is the result of the prototype of top chest we gonna show to the kid when we're gonna meet them.

**Universale's chair chest's top**
![](docs/images/t6.jpg)

**Aerospace's chest's top**
![](docs/images/t7.jpg)


When the kids came we quote some problems with the chest's tops:

-Some of them doesn't knew to read so the informations on the chest wasn't efficient.
-They often doesn't put back the top chest on it.
-Because the red color of the tops kids were looking more for the chest rather than the object. 

**Kids opening the chest and taking out the badges**
![](docs/images/enfant3.jpg)

Badges chest had been worked by Teajhay, more update on his git : https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/tejhay.pinheiroalbia/-/blob/main/docs/final-project.md


## **The reward** 

I first think that a coin could be great, like the memorial coins we get to a monument or things like that. I designed a coin that could be printed in 3D but also which could be cut . 

**Design of the first coin i made**
![](docs/images/jetondessin.jpg)

I made a hole (who wasn't big enough actually) into the coin, like that childrens can attach it to they're neck or they're bag with a little rope. 

**Coin i made with 1 mm Grey Card Board** 
![](docs/images/jeton1.jpg)

**Coin i made with 1 mm Grey Card Board** 
![](docs/images/jeton2.jpg)

Here are the settings i used to cut this coin.

**For the engraving I used the engraving mode with a power of 80% and a speed of 50%.**

**For the cutting I used the cutting mode with a speed of 50% and a power of 70% and a frequency of 100%**

_I could note that the engraving wasn't market enough and that the hole for the ring wasn't big enough._ 

I also made, like i said, a printed coin but it had a failure. The coin was too thick to be printed and while the impression it twisted. 
I decided to not keep using that method, the cost was of the production was to expensive for the number of coin we had to made. 

**Printed coin**
![](docs/images/jeton3.jpg)

After that we try to print those coin on a board of plexiglass. The render of the final product was way more clean and efficient. As well, the resistance of the plexiglass is way stronger that the grey card board, it's a good choice for a piece who will be bring to the home by the childs.

**Plexiglass coin**
![](docs/images/jeton4.jpg)

After a meeting with our proffesor, we head about a different choice. We thought that it would be a better idea to cut on a plexiglass board the shape of the object that the child are looking for through the museum. With this idea, the child would feel to be like a collectioner, come back to museum to collect all of the reward objects and have them all. Those shape will be also key rings, like that the child would not loose it straight ahead after getting out of the museum. 

As i said, we think that the shapes of the object could be more attractive for a kid than a basic coin, like that they could collect it one by one and came back to the museum to get more of those object's shape. 

We decide to add more detail to those shape to make them more attractive, here are the shapes i worked on.

**Blue endless chair's shape coin**
![](docs/images/b2.jpg)

**Universale chair's shape coin**
![](docs/images/b1.jpg)

**Master chair's shape coin**
![](docs/images/b3.jpg)

**Aerospace's shape coin**
![](docs/images/b4.jpg)

**Elephant chair's shape coin**
![](docs/images/b5.jpg)

**Air chair's shape coin**
![](docs/images/b6.jpg)

**Torch bunch's shape coin**
![](docs/images/b7.jpg)

We could now cut some of those shapes and see what the final coin could like and how we could improve the rendering process. As i said those shape will also be the key of the badges's box that we are gonna look about more in detail underneath.



**Blue endless chair's final shape coin**
![](docs/images/plug2.jpg)

I used different type of setting with an other object to be sure of the engraving and cutting process and which settings will be the most efficient. 


**First shape's cutting settings**
![](docs/images/set3.jpg)


**Second shape's cutting settings**
![](docs/images/set4.jpg)

**Third shape's cutting settings**
![](docs/images/set6.jpg)

