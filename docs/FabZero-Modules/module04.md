# 4. Computer aided cut

This week we had our formation about the aided computer cutting with a machine called Epilog. The main work of this formation is to design a lamp which will be cut into a propylène board.

## Prototype

As first i thought about the shapes that i wanted to give to my light. When i was a kid i had a lamp with a cone on it, but this lamp didn't had enought hole to let the light comes out. I wanted a cone lamp that remembers me the lamp that i had but with more efficiency that the one of my childhood.

I start by buying a lamp socket that i will use as the base of my lamp. Pre-fabricated lamp were forbidden even if we had to do only a lampshade. 

With that lampsocket i knew that the base of my lamp will need a 3.5 cm diameter hole in his center. For my prototype I used a canson paper, quite resistant to handle the weight of the prototype and the lamp itself.

**18.3 cm wide base**
![](../images/protodessin2.jpg)

After the base drew, i could now start to think about the sides of the lamp. I tried four different type of hole and engraving, one on each side of the lamp. Like that i could know which one was _better_ for the final result. 

I do two side with engraving lines. One the first picture engraving are space out by 1 cm and on the second engraving where space out by 2 cm 


**1cm space out engraving**
![](../images/protodessin1.jpg)

**2cm space out engraving**
![](../images/protodessin3.jpg)

On the two other side of the cone i tried a different technique. I wanted to create holes by cutting the matter and play on the full and the hollow. I took the same spacing than the engraving for those hollow.


**1 cm holes with 1 cm space out**
![](../images/protodessin6.jpg)

**2cm holes with 2 cm space out**
![](../images/protodessin4.jpg)

I created two tongues on every top cone's side like that i could connect the base of my lamp to her side. I assemble the all lamp to saw if dimension where good and to pick one of the four ornament. 

**1 cm engraving**
![](../images/proto2.jpg)

**2 cm engraving**
![](../images/proto1.jpg)

**1 cm hollow**
![](../images/proto4.jpg)

**2 cm hollow**
![](../images/proto3.jpg)



## Final Lamp

After have done the prototype, i could now take the measures of it and draw the final project on Autocad. I decided to keep my weft for the holes and engraving but i did the hollow randomly to make a final product less strict and adapt to the chidl that i was. 

I drew on different layers, like that the machine could acknowledge it and i could use different speed and power for the laser to have a different render. 

On the first layer, in yellow below, are the line that will be cut. In red are the line that are between cut and engraving, they will me fold, i have to care on the power and the speed of those line because it could ruined all the final product. In green are the engraving lines.

**Autocad file that i draw**
![](../images/dessin.jpg)

After this file done, i have to export it on a dwg to svg site. The epilog cutting machine is using inkscape software, inkscape need a svg file to work.

## The machine

The cutting machine that we are using at the ULB's Fablab is an Epilog made by Fusion Pro.  It is a realy usefull tool for architecture student, it allow you to cut with precision different mater from wood, paper, cardboard ... 

**Epilog by FusionPro**
![](../images/epilog.jpg)


On inkscape, I importe my svg file that i have to rescaling to be sure it his a good dimensions. 

**First try of the cutting machine and the power that i need**
![](../images/epilog2.jpg)

After done a short try of the machine on a fall of propylene (the matter I will use fort my lampshade), i have to calibrate the power and the speed of the laser. As i said, i made differents layers for differents use, i can pick every of those layers by choosing "color" in the menu. Now i can calibrate the power and speed for every layers, here are the settings:

Yellow layer: Cutting, speed **40%**, power **100%**, frequency **100%**

Red layer: Cutting, speed **60%**, power **40%**, frequency **100%**

Green layer: Cutting, speed **100%**, power **20%**, frequency **100%**

I could inspire myself for the settings on a test board and exemple board that are present in the fablab next to the epilog.


**Exemple cutting board**
![](../images/exemple.jpg)

After all the setting are good, i can now "print" my file. I choose epilog ingraver as a printer, i am redirected to the epilog software where i can use the integrated camera to place my draw perfectly. I have to give a name to the file, like that i can recognize it. Now the file is exported to the machine, i then have to launch the cut on the machine itself. 

**Before**__ launching the cutting process, i have to turn on the air extractor, it will get all the harmful smokes that can be created by the cutting process.
I then turn on the epilog by turning the key. I choose my file on the touchscreen and press play to launch the cut. The process took 32 second. 

I had a problem while i launch my cutting, i couldn't saw the laser in the machine, i ask to the fab manageuse and it was because my line where to thin, so the machine could not cut it. I have to thicken my line on inkscape and do all the process again.

**IT'S VERY IMPORTANT TO STAY DURING ALL THE PROCESS TO AVOID EVERY STARTING FIRE.**


Once the cutting process was over i could take back my propylene board, put the 38% of waster in the recycle bean for plastic and keep the final product.I plug every tong in they're compartment and here is the result.

![](../images/fini1.jpg)

![](../images/fini2.jpg)

![](../images/fini3.jpg)
