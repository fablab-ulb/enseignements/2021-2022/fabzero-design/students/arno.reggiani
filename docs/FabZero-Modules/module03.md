# 3. Impression 3D

This week I printed my object. 

## Research

Later in the week, i goes to the ULB's Fablab situated near Etterbeek's gare in the complex of the SeeU. With my laptop i exported my prusaslicer.

At first i have to dowload on prusa's software my 3D model with F3d file format. Then i place my object and duplicate it three times cause my project need it. 

![](../images/prusa.jpg)

After this part, i need to do all my settings. Here are all the settings i used:

**Layer's Height**     
_Layer Height_ : **0,2** mm    
the printer's nozzle is 0.4 mm so i have to take the half of it.  

**Vertical walls**  
_Perimeter_ : **3** (minimum)  

**Horizontal hulls**   
_solid layers_ : To the top : **3**    To the bottom : **3**   

**Quality (lower cuts)**
Detect perimeters making bridges -> **check** `V`   


    
**Filling**  
_Filling density_ : **do not exceed 25 %**  
_filling pattern_ : **choose** among the different patterns offered knowing that "Nid d'abeille" and "gyroide" are the strongest.
The filling's goal is to make stronger the object.

   
**Skirt**     
_Object's distance_ : **3** mm    
_Border width_ : **2** mm   
Main goal of width aim to solidify the object while it's printed, like that it can't fall.

  
**Supports**  
_Generate supports_ -> **check** `V`   
_Spacing pattern_ : **4** mm    

## 3D Printer

The printer that we are using at FabLab is a Prusa "Original i3 Mk3"

![](../images/imprimante.jpg)


At first, even before printing we have to clear the printing board with acetone. When we launch the print, we have to stay for the three or four layers to check if everything is good. If it's not we could like that stop the print, check the problems and relaunch the print. 

We have to load the thread. Important : we have to check that the thread is a PLA thread. PLA need a temperature of 215 c° at the nozzle and 60 c° to the board.

To upload my file on the Pruse i3 MK3 print i need to put my files on a SD card that i can after plug into the 3D printer. After i plugged it i can select my file and start printing it.
At first the nozzle will get heated like that the thread could flow easily, it then draw a line to expulse the olf Pla that where stuck in the nozzle last time.


![](../images/impression.jpg)


My print took 4 hour and 32 minutes. After that i had to unplugged the skirt and get clean my model.

![](../images/impression1.jpg)

![](../images/impression2.jpg)

Ps: sorry for my gross fingers on the pictures lmao.

After the impression, i notice that i had a problem with my object. One of the edges of my object was to thick so the print couldn't made a clear impression.

![](../images/defaut1.jpg)

![](../images/defaut2.jpg)

I will rework my model on Fusion 360, thicken this edge and reprint it soon.

