# 1. Documentation

This week I worked on defining my final project idea, started to getting used to the documentation process and had a lesson on how GitLab works and how to use it.

## Research

First of all we hed to create our git account and communicate our ID on a excel so the Fablab's administrator could add us on the class's GitLab.

On the 30 october 2021 we had our first teaching class with Denis Terwagne about GitLab, Linux, computers terminals and basics commands.
First we had to log in to our GitLab account. We were then introduce with the gitlab's interface, with our working place and folders. Then we were led to the tutorial that we had to follow to be able to pull our works from GitLab.com to our computer and then to push it in the opposite direction.

Tutorial's link (https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics/-/blob/master/documentation.md#why-is-documentation-important-)

First of all we download some text editor, in my case i have downloaded ATOM (i'm currently writing this topic with it). Then we downloaded GitLab for computer with GitBash, GitCMD and others, to be able to open the terminal. We could use linux and just type a command for downloading GitLab but unfortunately i'm on Windows lmao.

**Atom software**
![](../images/2.jpg)

After that, we're good to go. I open Windows Powershell as an administrator, like that i can type commands.

As first i have to the first setting like my mail, my pseudo etc...

![](../images/5.jpg)

Then i could configure my Gitlab. With the command "git config --global user.name "your_username"" i log my git to my account. I do the same with my mail with **git config --global user.email "your_email_address@example.com"**. To be sure everything where good we could type this command **git config --global --list**.

To connect my Gitlab to my laptop i have to create an SSH key. For that i had to install OpenSSH client and OpenSSH server.

![](../images/6.jpg)

After that i had to create my SSH key. Different type of SSH key could be create so as ED25519, RSA or DSA. I choose to create an ED25519 key.
To have the SSH key you have to generate it with that command **"ssh-keygen -t ed25519 -C "<comment>"**. Then the terminal will create a public key that you will use for gitlab and others site, and a private key that you must keep secret.
"Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/user/.ssh/id_ed25519):"
You have to specify a paraphrase then to be sure it's the good key.


After generate the SSH key, you have to encode it on gitlab.com. First of all on GitBash you have to enter this command for copying the SSHkey:
**"cat ~/.ssh/id_ed25519.pub | clip"**
Then you must paste it on you're GitLab profil. Go to you're profil, choose SSHkey on the left of you're screen. Then Paste the key. You have to choose when the key gonna start and end.

**"ssh -T git@gitlab.example.com"**

With the command above you can check if you're laptop is well connectec to you're Gitlab account.

Now you are able to clone a folders available on GitLab on you're laptop.
With the command under you gonna clone all the files in the **"sampleproject"** (this is for example). The url is available in u're folders, you can choose to clone it with SSH either with https.

**"git clone git@gitlab.com:gitlab-tests/sample-project.git"**

![](../images/7.jpg)

Now we can create a remote with the command, it will be the connector between the folder on you're computer and the git bash who will pull and push the files to gitlab.

**"git remote add origin git@gitlab.com:username/projectpath.git"**

You can now download files present on gitlab.com.

**"git pull <REMOTE> <name-of-branch>" **You have to name you're remote and name the branch that you want to download.

![](../images/8.jpg)

You can now saw the differences between files present on you're laptop and files present on GitLab with the command **git status**.

![](../images/10.jpg)

Before pushing my file i have to a commid thanks to the command **git add**

![](/docs/images/11.jpg)

After all you're work is done on you're computer you can now uptade it ont gitlab and let it be available to everyone with the command "git push origin main".

![](../images/1.jpg)       

I had a trouble with my picture and print screen, the visibility wasn't enough and the picture heigh where too low. I would thanks my dear friend Lucas Melon who let me use his printscreen for this module, thank you sir.
