# 5. Cut with a router

In this last module we had a formation about the router call Shaper. It's a digital and assisted milling machine. 

## Research

As first we had a presention of the machine and the safety rules that goes with it. 

**SharperTools's Origin**
![](../images/defonceuse.jpg)

We've been at first get introduce with the safety rules:

**-As first we need glass to avoid any shine.** 

**-The second important rules is to wear a mask to avoid any dust that could hurt u're lungs.**

**-It's important to have a plane and stable workplan.**

**-We need to stare the materials that we are cutting to the workplan.**


Specificity of the Shaper: 
The Shaper origins is a portable digital milling machine.
This machine allow us to upload our file on it and we can even draw with the machine. 
The machine is clamped to a depth of 43 mm (dependant of the collar and the mill used).  
We can use a collar of 8 mm or a collar 1/8 inch. 
The speed of the machine is variable between 10.000 and 26.000 turn minutes. It is important to choose the right speed, if we don't we could tear out some materials, so the cut wouldn't be clean. 
4 cutting mode are available: 
-Outside of the line
-On the line
-Inside the line
-Pocket
The most usefull mode is cuttin outside the line. 


Then we got introduce with how the shaper is working: We need to first get the file we want to cut as a svg file format with a usb key.
We can now place the ShaperTape on the materials we gonna cut: The ShaperTape is a tape that goes on the materials we have to cut, the machine's board camera will recognize the tape and will know where we gonna cut. It helps the machine having definise 2D plan. We need a 8cm space between each band of tape. The tape need to be befront the machine and not under it, if it was under she coundl't know where she is and it whill block herself, you woudln't be able to cut so.

To scan the new working space you will have to follow thoses steps:
-Scan the working space
-New working space 
-Push on the green button.

It will send scan the tape.

We can now place our design with the machine's touch screen. Once it's done, we have to follow fiew more steps before: At first we light up the vacuum to avoid any dust, then we light up the mill, after that the line that we want to cut gonna appear and we have to follow it. If we goes outside of this line the mill gonna get up and you won't be able to cut again, you have to push the green button to start again.

You can pause the cutting process with the orange button, the machine will remember where you already cut, push the green button to restart the cutting process when you're ready! 

The Shaper allow you to choose many settings like the outside or inside cuttint line, you can also choose an automode that allow you to let the machine moove the mill but you still have to follow the mill by pushing the shaper.
You can also choose the offset option settings: offset option setting is made for the angle of the pieces that you're cutting, you can round up the angles. It's very efficient when you have pieces that need to interlock each other.

We had in this formation a little lessons on how to change the mill:

As first we have to unplug the catch and turn off the mill, then we have to unscrew a crew present on the mill. We can now take off the all mill, a grey button is available near the head mill, we have to push it and unscrew with a key. We can now change the mill and reassemble everything.

Here is a picture of the piece of wood we trained on during this lesson.

![](../images/bois.jpg)
