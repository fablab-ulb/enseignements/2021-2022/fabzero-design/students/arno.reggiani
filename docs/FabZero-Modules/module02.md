## 2.Computer Aided-Design:

This week we had a lesson about Fusion 360 and a visit of the **Design Museum of Brussels**

First of all we check randomly somebody's work on GitLab, his advancement and maybe the problems he goes on.

After that we had our first lesson on Fusion 360. Fusion is a software that allow you to modelize an object in three dimension. The main goal of that is to be able to print it on a 3D printer. 

## Fusion 360

As i said above, Fusion 360 is a software of three dimension modelisation that allow you to create an object and to after that print it on a 3D printer. 
The point of the lessons was to discover this new software by creating an existant object. This object was designed by the teacher who give us this lesson. 
The object was a cube who got extruded in the center of three of his face. We had to then make a chamfer on the differents edges of the cube.
Under this description we gonna developpe all the path we followed when modelisating this object threw fusion360.

The first step of this work was to draw a square. 

![](../images/module_2_1.jpg)

Then we had to extrude this square to create a cube. 

![](../images/module_2_2.jpg)

When the cube is created in three dimension, we can select one of the cube's side and chamfer it to make it round like that it could look the same as the working exemple.

![](../images/module_2_3.jpg)

On one of the cube's side we are drawing a circle, this circle will be then extruded. We gonna repeated this operation two more times, at the end we had three holes in the cube.

![](../images/module_2_4.jpg)

![](../images/module_2_5.jpg)


The last step of this working excercice is to create three extruded part in the cube near the three extruded circle we just did. Again we gonna work on a cube's side, draw a rectangle and extrude it to the center. We have to repeat two times the operations to complete the excercice.

![](../images/module_2_6.jpg)


## Museum

In the afternoon of the same day i had to visit the Brussels's Design Museum. The main goal of this visit is to being accomodate to the museum and his pieces. After a short five weeks periods we gonna have to present the museum to differents group of childrens from nine to twelves years old. Like all childrens they gonna be bored realy fast so we have to interract with them and to distract them till they'll leave the museum. We have been asked to thought about some workshops who could feet with childrens and teach them how plastic works and to teach them in the same time the design. 

Here are some pictures of the museum : 

![](../images/musée_1.jpg)


![](../images/musée_2.jpg)


![](../images/musée_3.jpg)


![](../images/musée_4.jpg)


![](../images/musée_5.jpg)


![](../images/musée_6.jpg)


![](../images/musée_7.jpg)


## The object

In this museum we have been asked to choose an object present in the collection. We not gonna work on our own object we first choose in **module 1** because we can't split our time between this modelisation and project and between the museum and the workshops we have to think and create for the childrens. So i'm gonna create in three dimensions and then print with three dimension printer this object. 

I choose to work on a Rodolfo Bonneto's table made in 1969 in Italia. It's a red low table, it is a modulable table. Here are some pictures of the table i work with as a model.

![](../images/objet_1.jpg)


![](../images/objet_2.jpg)


![](../images/objet_3.jpg)


![](../images/objet_4.jpg)

## Modelisation

I have now to design and model the choosen object on Fusion 360. As first i drew the shape of the table before extrude it.

**The shape of the object**
![](../images/fusion1.jpg)

Then i could extrude the shape of the object, the main goal of that is to give height to it.

**extruded shape**
![](../images/fusion2.jpg)

I had to create a new plan based on the top of this extruded shape, like that i could draw the top of the table.

**New plan and draw of the top of the table**
![](../images/fusion3.jpg)

**Fianl top of the table**
![](../images/fusion4.jpg)

Then i had to do the hole that is present in the center of the table. For that i used a new plan situated on the top of the table. I drew the hole and then extrude it to the down of the object.

**Draw of the hole**
![](../images/fusion5.jpg)

**Extruded hole**
![](../images/fusion6.jpg)

Two things need to be done to end the design of this object, chamfer the edge and create tongues and compartement to had the possibility to stack each parts of the table.

**Chamfering the edge of the center hole**
![](../images/fusion7.jpg)

**Creating tongues and compartement**
![](../images/fusion8.jpg)

**Chamfer the edges of the table's top**
![](../images/fusion9.jpg)


I'm gonna print this object in the next week and keep my documentation work ON in the next [module](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/arno.reggiani/-/blob/main/docs/FabZero-Modules/module03.md). 



## 3D Models

![cube download](/docs/cube.f3d)


