Hello, this is my page.

## About me

![](docs/images/photo_profil.jpg)

Hi! My name is **Arno Reggiani**. I'm an architecture students based in Bruxelles. I'm actually studying in University Libre de Bruxelles also called ULB. I choose one option called FABLAB this year. In this option we are studying design in order to rethink and rebuild an object we liked. I choose to rethink a motocross helmet and to create a modular helmet.




## My background

![](docs/images/IMG_1116.jpg)


I am born the 29/07/1997 in the south of France, in a little city call La seyne sur mer, right next to sea. I basically grew up there till my 18 years old. Then i moove to Bruxelles for my studies and purchase my dream of bein and architect. 2 years ago i mooved back the Marseille while the Covid-19 was hitting hard our wold. I stayed to my parent's place for a while. I met my girlfriend at the same time and now we're living in Bruxelles again.

![](docs/images/laseyne.jpg)

I got a few hobbies to: i like to ride my bike, i had a few ones. I used to smoke weed but i stop it after having health issues, i had stress stokes and a depression but now i'm ok and i'm going forward. I like to spend my time on video games or watching a movie in the couch while it's raining. I'm also having joy while i'm cooking. 
In 2018 i start working as an self-employed as a building designer. I work for a constructor, who is also my dad's society.


### Project A

As a biker i start this project with a very simple report: Helmet are expensive and fragile. With just one hit the helmet is ruined. 
If you want a decent helmet with a nice design it cost you between 300$ to 500$ and sometime more than that. Ok i agree being a biker is an expensive passion. U have to buy a bike, pay for the maintains, pay for the gas and for a helmet. But for some people being a biker is not only a passion but a need. They need they're bike to go to the office, they need it to delivery people's meal or whatever. And most of the time they can't afford to buy a new helmet every one or two months if it take a hit, putting them in danger. 

So with this report i said myself: could you, with u're minimal experience in design, think and build a modular helmet with different pieces who's affordable and eco friendly? 
Based on my biker experiences and an old helmet that i have with me, that's what i'm gonna try do to in the followings weeks.

This helmet is a Fox V1 Creepin. I used to were it when i drove a Beta 50cc and a KTM 690 EXC. This helmet has had quite a few hits. If i had an accident with this helmet it wouldn't probably protect me.

![](docs/images/casque.jpg)

